package Graphics;
import javax.swing.*;
import java.awt.*;

//This program shows panels and layouts (Box, Flow and Grid)

//Create the components here

public class GUIExample2 extends JFrame { 
  
  JPanel pan1;
  JPanel pan2;
  
  //Create some GUI Components
  JLabel nameLabel;
  JTextField nameField;
  JButton yesButton; 
  JButton noButton; 
  JButton maybeButton;  
  JComboBox donuts;
  
  // Constructor - Setup your GUI
  public GUIExample2() { 
    // CREATE GUI OBJECTS
    pan1 = new JPanel();  //Create a Panel
    pan2 = new JPanel();  //Create another Panel
    
    //Create some GUI Components
    nameLabel = new JLabel("Name: ", JLabel.RIGHT); 
    nameField = new JTextField(10);
    yesButton = new JButton("Yes  ");  
    noButton = new JButton("No   ");  
    maybeButton = new JButton("Maybe");  
    donuts = new JComboBox<String>();
    
    // SET UP YOUR FRAME AND LINK ALL COMPONENTS
    setTitle("Choose your donut!");  //Create a window with a title
    setSize(320, 240);         //Set the size of the window
    setResizable(false);     // Do not llow the user to resize the window - locks it so that buttons are all in place
    
    FlowLayout layout1 = new FlowLayout();   //Create a FlowLayout
    GridLayout layout2 = new GridLayout();   //Create a GridLayout
    BoxLayout layout3 = new BoxLayout(pan2,BoxLayout.Y_AXIS); // Create a BoxLayout for pan1
    
    setLayout(layout1);  // Set the layout of the FRAME to flow - means the panels I place will go as flow
    
    pan1.setLayout(layout1);  //Set the layout of the 1st PANEL to Flow
    pan2.setLayout(layout3);  //Set the layout of the 2nd PANEL to Box
    
    // Add some components to panel 1
    pan1.add(nameLabel);
    pan1.add(nameField);
    
    //Add items to the combo box before adding it to the panel
    donuts.addItem("Chocolate");
    donuts.addItem("Glazed");
    donuts.addItem("Sparkles");
    pan1.add(donuts);
    
    // Add some components to Panel 2
    pan2.add(yesButton);
    pan2.add(noButton);
    pan2.add(maybeButton);
    
    
    // Add both PANELS to the FRAME
    add(pan1);
    add(pan2);
    
    setVisible(true);  // Display the GUI
  }
  
  public static void main(String[] args) {   // MAIN METHOD
	  EventQueue.invokeLater(()->{ 
		  GUIExample2 frame1 = new GUIExample2();  // Start the GUI
		  frame1.setVisible(true);
	  });
  }
  
}