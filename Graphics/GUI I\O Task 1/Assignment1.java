package Graphics;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class Assignment1 extends JFrame implements ActionListener{

	JButton pressButton;
	JTextField name;
	JTextField Message;
	JLabel label1;
	JLabel label2;
	public Assignment1() {
		init();
	}
	private void init() {
		pressButton = new JButton("Enter");
		pressButton.addActionListener(this);
		label1 = new JLabel("Enter name here", JLabel.LEFT);
		label2 = new JLabel("Message              ", JLabel.LEFT);
		setTitle("Hello Name Program");
		setSize(800, 600);
		
		FlowLayout fl = new FlowLayout();
		fl.setAlignment(FlowLayout.LEFT);
		setLayout(fl);
		name = new JTextField("", 50);
		Message = new JTextField("", 50);
		name.addActionListener(this);
		
		add(label1);
		add(name);
		add(pressButton);
		add(label2);
		add(Message);
		
	}
	public static void main(String[]args) {
		EventQueue.invokeLater(()->{
			Assignment1 run = new Assignment1();
			run.setVisible(true);
		});
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Enter")) {
			Message.setText("Hello " + name.getText());	
		}
	}
}
