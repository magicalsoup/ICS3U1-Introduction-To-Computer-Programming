package Graphics;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class Assignment2 extends JFrame implements ActionListener{

	public Assignment2() {
		init();
	}
	JLabel label1;
	JLabel label2;
	JLabel label3;
	JTextField input1;
	JTextField input2;
	JTextField output;
	JButton okButton;
	
	private void init() {
		FlowLayout fl1 = new FlowLayout();
		fl1.setAlignment(FlowLayout.LEFT);
		setLayout(fl1);
		
		
		label1 = new JLabel("Enter Length of Side A:", JLabel.LEFT);
		label2 = new JLabel("Enter Length of Side B:", JLabel.LEFT);
		label3 = new JLabel("Hypotenuse Length:", JLabel.LEFT);
		
		input1 = new JTextField("", 55);
		input2 = new JTextField("", 50);
		
		output = new JTextField("Answer", 50);
		okButton = new JButton("OK");
		okButton.addActionListener(this);
		
		
		setSize(800, 600);
		setTitle("Hypotenuse Calculator");
		add(label1); add(input1);
		add(label2); add(input2);
		add(okButton); 
		add(label3); add(output);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("OK")) {
			double a=0, b=0;
			try {
				a = Double.parseDouble(input1.getText());
				b = Double.parseDouble(input2.getText());
				double c = getHypotenuse(a, b);
				output.setText(String.valueOf(c));
			}
			catch(Exception t) {
				System.out.println("Error");
			}
		
			
		}
	}
	public static double getHypotenuse(double a, double b) {
		return Math.sqrt((a*a) + (b*b));
	}
	public static void main(String[]args) {
		EventQueue.invokeLater(()->{
			Assignment2 run = new Assignment2();
			run.setVisible(true);
		});
	}
	
}
